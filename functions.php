<?php 

function functions_includes()
{
    // Here we load from our includes directory
    // This considers parent and child themes as well    
    locate_template( array( 'functions-includes/add-theme-support.php' ), true, true );
    locate_template( array( 'functions-includes/custom-post-types.php' ), true, true );
    locate_template( array( 'functions-includes/taxonomies.php' ), true, true );
}
add_action( 'after_setup_theme', 'functions_includes' );